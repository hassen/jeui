# JEUI
#### 项目介绍

九厚科技工作室出品  JEUI---优雅的管理后台前端框架

 ![Image text](https://gitee.com/hassen/jeui/raw/master/static/a1.png)

 演示地址 https://hassen.gitee.io/jeui 用户名 admin 密码 123456

#### 使用场景

1. 前后完全分离的后台管理项目
2. 需要快速搭建一整套完成的后台前端框架(登录，session管理，异步请求，数据展示，路由管理等)
3. 现在前后端没有分离系统的改装
4. 适合前端人员完成搭建，因为内部包含mock数据模拟，切换正式环境，去掉mock依赖即可。与真实后端无缝集成。


#### 前端技术
1. Vue         渐进式前端框架
2. Element UI  UI框架
3. Vue-router 路由跳转
4. Axios  ajax请求
5. mock   数据模拟

#### 细节描述
1. axois-mock-adapter 完成axios的数据拦截
2. nprogress 路由跳转进度条
3. elementui loading  实现ajax的异步请求遮罩
4. main.js 完成路由跳转是是否登录的检查
5. base.js 完成ajax请求时是否登录的检查
6. vuex暂时未用，待定

#### 目录结构

├─build  
├─config  
├─dist               //run build的输出目录   
│  └─static  
│      ├─css  
│      └─js  
├─src  
│  ├─api            //api的存放目录，后台请求的api  
│  ├─assets         //资源文件，比如图片等  
│  ├─components     //vue文件  
│  ├─mock           //mock 对axios请求拦截，并返回数据  
│  │  └─data  
│  ├─styles         //自定义样式  
│  └─vuex  
├─static  
└─test  
    ├─e2e  
    │  ├─custom-assertions  
    │  └─specs  
    └─unit  
        └─specs  

#### 安装教程
1. git clone https://gitee.com/hassen/jeui
2. npm install
3. npm run dev
4. 浏览器输入 http://localhost:8080/ 访问


#### 二开步骤
1. 在components目录下创建自己业务的vue文件，仿照user.vue
2. 在api目录下定义自己的api请求，模仿api-user.js
3. 在routes.js中增加路由数据


#### 本项目孵化过程

1. 添加淘宝镜像 以及vue-cli  
npm install -g cnpm --registry=https://registry.npm.taobao.org  
npm install -g vue-cli  
2. 初始化项目  
vue init webpack jeui  
里面包含 项目名称 测试 等选择，最后包含执行npm install  
3. 安装其他用的包
cnpm i element-ui -S  
4. 包含组件
axios   
vue-router  
vuex  
mock  
babel-polyfill  
node-sass  
sass-loader  
axois-mock-adapter  
nprogress  

#### 参与贡献
九厚科技工作室倾情奉献


#### 码云特技

