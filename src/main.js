import 'element-ui/lib/theme-chalk/index.css'
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'

import VueRouter from 'vue-router'
import store from './vuex/store'
import Vuex from 'vuex'
// import NProgress from 'nprogress'
// import 'nprogress/nprogress.css'

import Mock from './mock/index'

import routes from '@/router/index.js'
import dd from '@/common/js/data.js'
import api from '@/api'

import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

Mock.bootstrap()

// import 'font-awesome/css/font-awesome.min.css'

Vue.use(ElementUI, { size: 'mini', zIndex: 3000 })
Vue.use(VueRouter)
Vue.use(Vuex)

Vue.prototype.dd = dd // 静态常量
Vue.prototype.api = api

Vue.component('XX', {
  props: ['title'],
  template: '<a href="#">{{title}}</a>'
})

NProgress.inc(0.2)
NProgress.configure({ easing: 'ease', speed: 500, showSpinner: false })

// NProgress.configure({ showSpinner: false });

const router = new VueRouter({
  mode: 'history',
  base: '/jeui/',
  routes
})

debugger

router.beforeEach((to, from, next) => {
  NProgress.start()
  if (to.path === '/login') {
    sessionStorage.removeItem('user')
  }
  let user = JSON.parse(sessionStorage.getItem('user'))
  if (!user && to.path !== '/login') {
    next({ path: '/login' })
  } else {
    document.title = to.name
    next()
  }
})

router.afterEach(() => {
  NProgress.done()
})

new Vue({
  // el: '#app',
  // template: '<App/>',
  router,
  store,
  // components: { App }
  render: h => h(App)
}).$mount('#app')
