import HelloWorld from '@/views/HelloWorld'
import Layout from '@/views/Layout'
import Login from '@/views/Login'
import User from '@/views/User'
import NotFound from '@/views/404'
import test from '@/views/test'

import vuexset from '@/views/vuexset'
import vuexget from '@/views/vuexget'

import dashboard from '@/views/echart/index'

let routes = [{
  path: '/404',
  name: '404',
  component: NotFound,
  hidden: true
},
{
  path: '/login',
  name: 'login',
  component: Login,
  hidden: true

},
{
  path: '/test',
  name: 'test',
  component: test,
  hidden: true

},
{
  path: '/',
  name: '主页1',
  redirect: { path: '/home' },
  hidden: true
},
{
  path: '/dashboard',
  name: '监控中心',
  component: Layout,
  icon: 'el-icon-upload',
  redirect: { path: '/dashboard/dashboard' },
  top: true,
  children: [{
    path: '/dashboard/dashboard',
    name: '监控中心2',
    component: dashboard
  }]
},

{
  path: '/vuex',
  name: 'vuex使用',
  icon: 'el-icon-rank',
  component: Layout,
  redirect: { path: '/vuex/set' },
  children: [{
    path: '/vuex/set',
    name: '设置vuex',
    component: vuexset
  }, {
    path: '/vuex/get',
    name: '获取vuex',
    component: vuexget
  }]
},
{
  path: '/home',
  name: '主页',
  icon: 'el-icon-menu',
  component: Layout,
  redirect: { path: '/user' },
  children: [{
    path: '/user',
    name: '用户管理',
    component: User
  }, {
    path: '/404inner',
    name: '404页面',
    component: NotFound
  }]
},
{
  path: '*',
  hidden: true,
  redirect: { path: '/404' }
}
]

export default routes
