import axios from 'axios'

let user = 'user'

export const batchRemoveUser = params => { return axios.get(`${user}` + '/batchremove', { params: params }) }
export const removeUser = params => { return axios.get(`${user}` + '/remove', { params: params }) }
export const editUser = params => { return axios.get(`${user}` + '/edit', { params: params }) }
export const addUser = params => { return axios.get(`${user}` + '/add', { params: params }) }
export const getUserListPage = params => { return axios.get(`${user}` + '/listpage', { params: params }) }
export const login = params => { return axios.post('/login2', params) }

export const consumeListChart = params => { return axios.get(`${user}` + '/consume_list_chart', { params: params }) }
