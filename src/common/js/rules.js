var QQV = (rule, value, callback) => {
  if (value === '') {
    callback(new Error('输入QQ号'))
  } else if (/^[1-9][0-9]{4,10}$/.test(value)) {
    callback()
  } else {
    callback(new Error('输入正确的QQ号'))
  }
}

// 类似金钱,首位不为0,最多2位小数
export function checkNumPot2 (rule, value, callback) {
  const reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/
  if (!value) {
    return callback(new Error('请填写数字'))
  } else if (!reg.test(value)) {
    return callback(new Error('请填写数字,最多2位小数'))
  } else {
    callback()
  }
}

// 身份证
export function checkIdNum (rule, value, callback) {
  const reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/
  if (!value) {
    return callback(new Error('证件号码不能为空'))
  } else if (!reg.test(value)) {
    return callback(new Error('证件号码不正确'))
  } else {
    callback()
  }
}

// 整数
export function checkInterNum (rule, value, callback) {
  const reg = /^[0-9]*[1-9][0-9]*$/
  if (!value) {
    return callback(new Error('请填写整数'))
  } else if (!reg.test(value)) {
    return callback(new Error('请输入整数'))
  } else {
    callback()
  }
}

// 密码规则
// let passwordReg = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{6,20}$/
let passwordReg = /^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/

export default {
  QQ: [{ required: true, validator: QQV, trigger: 'blur' }],
  mobile: [{ required: true, pattern: /^1[34578]\d{9}$/, message: '手机号码格式不正确', trigger: 'blur' }],
  mobileAuthCode: [{ required: true, pattern: /^\d{6}$/, message: '验证码格式不正确', trigger: 'blur' }],

  password: [{ required: true, pattern: passwordReg, message: '密码格式不正确', trigger: 'blur' }],
  password2: [{ required: true, pattern: passwordReg, message: '密码格式不正确', trigger: 'blur' }],
  newPassword: [{ required: true, pattern: passwordReg, message: '密码格式不正确', trigger: 'blur' }],
  numPot2: [{ required: true, validator: checkNumPot2, trigger: 'blur' }],
  amount: [{ required: true, validator: checkNumPot2, trigger: 'change' }],
  InterNum: [{ required: true, validator: checkInterNum, trigger: 'blur' }],
  userName: [{ required: true, message: '请输入瑞云账号', trigger: 'change' }]
}
