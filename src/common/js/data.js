const menu = [
  {
    name: '账户中心',
    url: '/account',
    icon: 'raysync-account',
    id: 0
  },
  {
    name: '充值记录',
    url: '/rechargelist',
    icon: 'raysync-recharge',
    id: 1
  },
  {
    name: '消费清单',
    url: '/consumelist',
    icon: 'raysync-consume',
    id: 2
  },
  {
    name: '分享与邀请',
    url: '/sharelist',
    icon: 'raysync-share2',
    id: 3
  },
  {
    name: '安全中心',
    url: '/securitycenter',
    icon: 'raysync-safety',
    id: 4
  }
]
const pageSize = 5
export default {menu, pageSize}
// 静态常量数据
